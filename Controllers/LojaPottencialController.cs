using Microsoft.AspNetCore.Mvc;
using desafioPottencial.Context;
using desafioPottencial.TabelaSQL_Entities;
//Uma controller nada mais é que uma classe que vai agrupar as suas requisições http e vai disponibilizar os seus end points.
namespace desafioPottencial.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LojaPottencialController : ControllerBase
    {
        private readonly LojaPottencialContext _context;

        public LojaPottencialController(LojaPottencialContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Create(LojaPottencial lojaPottencial)
        {
            _context.Add(lojaPottencial);
            _context.SaveChanges();
            return Ok(lojaPottencial);
        }
    }
}