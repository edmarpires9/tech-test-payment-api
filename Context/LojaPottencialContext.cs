using Microsoft.EntityFrameworkCore;
using desafioPottencial.TabelaSQL_Entities;

namespace desafioPottencial.Context
{
    public class LojaPottencialContext : DbContext
    {
        public LojaPottencialContext(DbContextOptions<LojaPottencialContext> options) : base(options)
        {

        }

        //Entidade nome do Banco de dados.
        public DbSet<LojaPottencial> TabelaVenda { get; set; }
    }
}