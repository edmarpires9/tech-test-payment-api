namespace desafioPottencial.TabelaSQL_Entities
{
    public class LojaPottencial
    {
        public int id { get; set; }
        public string nomeVendedor { get; set; }
        public string itemVendido { get; set; }
    }
}